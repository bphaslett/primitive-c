***********************  
Primitive C classes   
***********************  

Preferred ordering is:  classes, data sections, method prototypes, methods  

(required)  
Define a class with CLASS(class_name).  

 e.g.  

    CLASS(std);
  
  
(required)  
Define the class data section with DATA(class_name).  
This works just like a struct.  

 e.g.  

    DATA(std) {
    ...
    }
  
  
(required)  
Initialize your classes with INIT(CLASS).  
This must be done with all classes, and it initializes the namespace.  
One place you can do this, for example, is in main.  

 e.g.  

    int main()
    {
        ...
        INIT(std);
    }
  
  
Access class data with D(name, class).  

 e.g.  

    DATA(std) {
        int x;
    }
    ...

    n = D(x, std);
    D(x, std)++;
    if (D(x, std) == 123) { do_something(); }
  
  
Declare a method prototype with DECLARE_METHOD(name, class) in the same manner  
as an ordinary function prototype.  

 e.g.  

    DECLARE_METHOD(printHelp, std);
  
  
Define the method body with DEFINE_METHOD(name, class) in the same manner  
as an ordinary function definition.  
  
 e.g.  
  
    DEFINE_METHOD(printHelp, std)
    {
    ...
    }
  
  
Call a method with CALL_METHOD(name, class).  
Internally, methods work just like normal C functions, except the return  
type is void, and the only argument actually passed is a class reference.  
Classes call their own methods, and all data is accessed through the  
class' data section.  While the method is being called, it is locked in a  
pthread mutex.  

 e.g.  

    CALL_METHOD(printHelp, std);
  
  
*******
FRIENDS
*******

Define a friend class in a class data section with FRIEND(name).  
That friend class must be defined elsewhere with CLASS(name), and later initialized.  

 e.g.  

    CLASS(std);
    CLASS(nonstd);
    
    DATA(std) {
        ...
        FRIEND(nonstd);
    }
     
    DATA(nonstd) {
        int foo;
    }
  
  
After your class and friend class are both initialized, a "handshake" must be made to access their data.  
This is done with FRIEND_SHAKE(class, friend).  

 e.g.  

    INIT(std);
    INIT(bar);
    FRIEND_SHAKE(std,bar);
  
  
Call a friend's method with CALL_METHODF(name, class, friend).  

 e.g.  

    CALL_METHODF(printHelp, std, error);
  
  
Access friend class data with DF(name, class, friend).  

 e.g.  

    some_var = DF(foo, std, bar);
  
  
****************  
Custom Types  
****************  

bool (simple true/false)  

 e.g.  

    CLASS(std) {
        bool has_opt;
    }
    ...
        D(has_opt)=false;

