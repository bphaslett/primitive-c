/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * find most/least significant bits for a given value
 * (uses primitive classes)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* the max value is limited due to usage of atol */

#include <stdio.h>    // for printf
#include <stdint.h>   // for uint64_t etc
#include <stdlib.h>   // for exit
#include <string.h>   // for strcpy
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <ctype.h>    // for isdigit
#include <stdbool.h>  // for bools
#include "primitive_c.h"   // primitive c classes

#define N_BITS 64

#define FAIL_INVALID \
  ({ \
    fprintf(stderr, "Error, input is invalid.\n"); \
    exit(EXIT_FAILURE); \
  })

#define ITERATE \
  ({ uint64_t _x=x; \
    if (user_input & (1ULL << _x)) {  \
      user_input = 1UL<<_x; \
      break; \
    }; \
  })

#define PRINT_MATCH \
  ({ \
    DF(user_input,std,printing) = user_input; \
    printf("base10:%lu, base2:", user_input); \
    CALL_METHODF(display_bits, std, printing); \
  })

static struct option long_options[] = {
  { "var", 1, NULL, 'v' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

CLASS(std);
CLASS(printing);
DATA(printing) {
  uint64_t user_input;
};
DATA(std) {
  uint8_t *str1;
  uint32_t str1_size;
  uint64_t user_input;
  FRIEND(printing);
};
DECLARE_METHOD(find_msb,std);
DECLARE_METHOD(find_lsb, std);
DECLARE_METHOD(filter_input, std);
void print_help();
DECLARE_METHOD(display_bits, printing);

int main(int argc, char *argv[])
{
  bool has_var = false;
  int opt;
  INIT(std);
  INIT(printing);
  FRIEND_SHAKE(std,printing);

  if (argc < 2) {
    CALL_METHOD(print_help, std);
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt_long(argc, argv, "v:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'v':
        D(str1_size,std)=strlen(optarg);
        D(str1,std) = malloc(D(str1_size+1,std));
        strcpy(D(str1,std), optarg);

        CALL_METHOD(filter_input, std);
        D(user_input,std) = atol(D(str1,std));
        has_var = true;
        break;
      case 'h':
        CALL_METHOD(print_help, std);
        exit(EXIT_SUCCESS);
        break;
    }
  }

  if (has_var == true) {
    CALL_METHOD(find_lsb, std);
    CALL_METHOD(find_msb, std);
  }

  if (D(str1,std)) { free(D(str1,std)); }
  return 0;
}

DEFINE_METHOD(filter_input, std)
{
  uint32_t i;

  for (i=0; i<D(str1_size,std); i++) {
    if (!isdigit(D(str1[i],std)))
      FAIL_INVALID;
  }

}

DEFINE_METHOD(find_lsb, std)
{
  uint64_t user_input = D(user_input,std);
  uint64_t x;

  if (user_input==0)
    FAIL_INVALID;

  for (x=0; x<=(N_BITS-1); x++)
    if (user_input & (1ULL << x)) 
      ITERATE;

  puts("matched least significant bit");
  PRINT_MATCH;
}

DEFINE_METHOD(find_msb, std)
{
  uint64_t user_input = D(user_input,std);
  uint64_t x;

  if (user_input==0)
    FAIL_INVALID;

  for (x=(N_BITS-1); x>=0; x--)
    if (user_input & (1ULL << x)) 
      ITERATE;

  puts("matched most significant bit");
  PRINT_MATCH;
}

DEFINE_METHOD(display_bits, printing)
{
  char bits[64];
  int32_t i, hold;
  size_t sz;
  uint64_t digit = D(user_input,printing);

  // build an array of bits
  for (i=0; digit>0; i++) {
    sz = i+2;
    hold=digit%2;
    sprintf(&bits[i], "%u", hold);
    digit/=2;
  }
  
  // print array backwards
  for (i = sz-2; i >= 0; i--) {
    printf("%c", bits[i]);
  }
  puts("");
}

void print_help()
{
  puts("msb");
  puts("find most and least significant bits\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -v, --var[=number]\tinput value (base-10) [0-4611686018427387904]");
}
