/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * do stuff with prime numbers like
 * factor primes out of a number
 * determine if number is prime
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>    // for printf
#include <stdint.h>
#include <stdlib.h>   // for exit
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <stdbool.h>
#include "primitive_c.h"   // primitive c classes

static struct option long_options[] = {
  { "prime", 0, NULL, 'p' },
  { "factor", 0, NULL, 'f' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

CLASS(std);
DATA(std) {
  bool found_prime;
  unsigned long factor;
  unsigned long prime;
};

void print_help();
DECLARE_METHOD(factor_primes, std);
DECLARE_METHOD(find_primes, std);

int main(int argc, char *argv[])
{
  bool select_factors=false, select_primes=false;
  int opt, rv;
  INIT(std);

  if (argc < 2) { goto error; }

  while ((opt = getopt_long(argc, argv, "p:f:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'p':
        rv = sscanf(optarg, "%lu", &D(prime,std));
        if (rv == 1) { select_primes=true; }
        break;
      case 'f':
        rv = sscanf(optarg, "%lu", &D(factor,std));
        if (rv == 1) { select_factors=true; }
        break;
      case 'h':
        CALL_METHOD(print_help, std);
        exit(EXIT_SUCCESS);
    }
  }

  if (select_primes==true) {
    CALL_METHOD(find_primes, std);
  }
  if (select_factors==true) {
    CALL_METHOD(factor_primes, std);
  }

  goto end;

error:
  CALL_METHOD(print_help, std);
  exit(EXIT_FAILURE);

end:
  return 0;
}

DEFINE_METHOD(factor_primes, std)
{
  unsigned long divisor=2, rem;

  while (divisor < D(factor,std)) {
    rem = D(factor,std) % divisor;

    if (rem==0) {
      printf("%lu ", divisor);
      D(factor,std)/=divisor;
      divisor=2;
    } else {
      divisor++;   
    }
  }

  printf("%lu\n", D(factor,std));
}

DEFINE_METHOD(find_primes, std)
{
  D(found_prime,std)=true;
  unsigned long a, b=D(prime,std), c=0;

  for (a=2; a<b; a++) {
    c=b%a;
    if (c==0) {
      D(found_prime,std)=false;
      break;
    }
  }

  if (D(found_prime,std)==true) { printf("%lu is prime\n", b); }
  else { printf("%lu is not prime\n", b); }
}


void print_help()
{
  puts("primes");
  puts("does stuff involving prime numbers\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -f, --factor[=NUM]\tfactor primes out of NUM");
  puts("  -p, --prime[=NUM]\tdetermine if NUM is prime");
}
