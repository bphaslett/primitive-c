/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * test out low-level ioport and iopl access
 * (using primitive c classes)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>    // for printf
#include <stdlib.h>   // for exit
#include <string.h>   // for strerror_r
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <sys/io.h>   // for ioperm, iopl
#include <errno.h>    // for errno
#include <stdbool.h>
#include "primitive_c.h"

#define ALLOW 1
#define DENY 0
#define BUFLEN 256

static struct option long_options[] = {
  { "smm", 0, NULL, 's' },
  { "iopl", 1, NULL, 'l' },
  { "ioport", 1, NULL, 'p' },
  { "cs", 0, NULL, 'c' },
  { "flags", 0, NULL, 'f' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

CLASS(std);
CLASS(error);

DATA(std) {
  unsigned int num;   // ioports
  unsigned int lvl;
  FRIEND(error);
};
DATA(error) {
  int buf_len;
  int errsv;
  char *buf;
};

DECLARE_METHOD(test_perm, std);
DECLARE_METHOD(set_iopl, std);
DECLARE_METHOD(test_max_port, std);
DECLARE_METHOD(test_smm, std);
void print_help();
void print_binary(unsigned long);
void print_cs_register();
void print_flags();

int main(int argc, char *argv[])
{
  bool has_port = false, has_level = false, has_smm = false, do_cs = false, do_flags = false;
  int opt;
  INIT(std);
  INIT(error);
  FRIEND_SHAKE(std,error);
  D(lvl,std)=3;

  if (argc < 2) {
    CALL_METHOD(print_help, std);
    exit(EXIT_FAILURE);
  }
  D(buf, error) = malloc(BUFLEN);
  D(buf_len, error) = BUFLEN-1;

  while ((opt = getopt_long(argc, argv, "cfsp:l:b:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'f':
        do_flags = true;
        break;
      case 'c':
        do_cs = true;
        break;
      case 'p':
        has_port = true;
        D(num,std) = atoi(optarg);
        break;
      case 'l':
        has_level = true;
        D(lvl,std) = atoi(optarg);
        break;
      case 's':
        has_smm = true;
        break;
      case 'h':
        CALL_METHOD(print_help, std);
        break;
    }
  }

  if (has_level == true) {
    CALL_METHOD(set_iopl, std);
  }

  if (has_port == true) {
    CALL_METHOD(test_max_port, std);
    CALL_METHOD(test_perm, std);
  }

  if (has_smm == true) {
    CALL_METHOD(test_smm, std);
  }

  if (do_flags == true) {
    CALL_METHOD(print_flags, std);
  }

  if (do_cs == true) {
    CALL_METHOD(print_cs_register, std);
  }

  free(D(buf, error));
  return 0;
}

DEFINE_METHOD(test_perm, std)
{
  unsigned int io_port;

  puts("***********************************");
  puts("* Testing Permission for IO Ports *");
  puts("***********************************");
  for (io_port = 0; io_port < D(num,std); io_port++) {

    ioperm(io_port, 2048, ALLOW);
    DF(errsv, std, error) = errno;

    if (DF(errsv, std, error) != 0) { 
      strerror_r(DF(errsv, std, error), DF(buf, std, error), DF(buf_len, std, error));
      fprintf(stderr, "%5u: error (%s)\n", io_port, DF(buf, std, error));
      exit(EXIT_FAILURE);
    } else {
      printf("%5u: success\n", io_port);
    }

  }

}

DEFINE_METHOD(set_iopl, std)
{

  puts("******************************");
  puts("* Setting IO Privilage Level *");
  puts("******************************");

  iopl(D(lvl,std));
  DF(errsv,std,error) = errno;

  if (DF(errsv,std,error) != 0) {
    strerror_r(DF(errsv,std,error), DF(buf,std,error), DF(buf_len,std,error));
    fprintf(stderr, "%5u: error (%s)\n", D(lvl,std), DF(buf,std,error));
    exit(EXIT_FAILURE);
  } else {
    printf("%5u: success.\n", D(lvl,std));
  }
}

DEFINE_METHOD(test_max_port, std)
{
  if (D(num,std) > 65536 ||
    D(num,std) < 0) {
    fprintf(stderr, "Error: number of ports to test is from 1 to 65536 \n");
    exit(EXIT_FAILURE);
  }
}
DEFINE_METHOD(test_smm, std)
{
  // can only be entered with iopl set to 3
  int c;
  printf("Testing System Management Mode with lvl %d..\n", D(lvl,std));
  // SMM port 0xb2
  asm("mov $0xb2, %dx;"
    "mov $0x1, %al;"
    "outb %al, %dx;");

  asm("rsm;");

}

void print_cs_register()
{
  unsigned long cs=0;

  asm("mov %%cs, %0;" : "=r" (cs));

  printf("CS register: ");
  print_binary(cs);

}

void print_flags()
{
  unsigned long flags=0;

  asm("pushf;"
      "pop %0;"
      : "=r" (flags));

  printf("FLAGS register: ");
  print_binary(flags);
}

void print_binary(unsigned long var)
{
  while (var>0) {
    printf("%lu", var & 0x1);
    var>>=1;
  }
  puts("");
}

void print_help()
{
  puts("io-test");
  puts("test out low-level ioport and iopl access\n");
  puts("Usage: io-test [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -p, --ioport[=NUM]\thow many IO ports to test (1-65536)");
  puts("  -l, --iopl[=NUM]\tset IO Privilage Level (0-3)");
  puts("  -s, --smm\t\ttest system management mode");
  puts("  -c, --cs\t\tprint cs register");
  puts("  -f, --flags\t\tprint flags register");
}
