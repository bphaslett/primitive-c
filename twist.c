/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * twist -- generate twist table from kernel's drivers/char/random.c
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdio.h>    // for printf, sscanf
#include <stdint.h>
#include <stdlib.h>   // for exit
#include <string.h>   // for strlen
#include <getopt.h>   // for getopt, getopt_long, struct option
#include <stdbool.h>
#include "primitive_c.h"   // primitive c classes


#define rol32(word, shift)  ({ \
  uint32_t _word = word; \
  __asm__ __volatile__ ("rol %%cl, %%eax;" : "=a" (_word) : "a" (_word), "c" (shift) : ); \
  _word; })

#define ror32(word, shift) ({ \
  uint32_t _word = word; \
  __asm__ __volatile__ ("ror %%cl, %%eax;" : "=a" (_word) : "a" (_word), "c" (shift) : ); \
  _word; \
})

static struct option long_options[] = {
  { "decimal", 0, NULL, 'd' },
  { "hexadecimal", 0, NULL, 'H' },
  { "iterations", 1, NULL, 'i' },
  { "value1", 1, NULL, 'a' },
  { "value2", 1, NULL, 'b' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

enum __attribute__ ((__packed__)) format_types {
  HEX,
  DEC
};

CLASS(std);
DATA(std) {
  enum format_types format;
  uint32_t value1;
  uint32_t value2;
  int iterations;
};

void print_help();
DECLARE_METHOD(twist_table, std);

int main(int argc, char *argv[])
{
  bool has_a = false, has_b = false;
  int opt;
  char *str1, *str2;
  INIT(std);
  D(format,std)=HEX;
  D(iterations,std)=1;

  if (argc < 2) { goto error; }

  while ((opt = getopt_long(argc, argv, "dHi:a:b:h", long_options, NULL)) != -1) {
    switch (opt) {
      case 'd':
        D(format,std)=DEC;
        break;
      case 'a':
        str1 = malloc(strlen(optarg)+1);
        strcpy(str1,optarg);
        has_a = true;
        break;
      case 'b':
        str2 = malloc(strlen(optarg)+1);
        strcpy(str2, optarg);
        has_b = true;
        break;
      case 'i':
        D(iterations,std) = atoi(optarg);
        break;
      case 'h':
        CALL_METHOD(print_help, std);
        exit(EXIT_SUCCESS);
    }
  }

  if (has_a == true) {
    switch (D(format,std)) {
      case DEC: 
        sscanf(str1, "%u", &D(value1,std));
        break;
      case HEX:
        sscanf(str1, "%x", &D(value1,std));
        break;
    }

    if (has_b == true) {
      switch (D(format,std)) {
        case DEC:
          sscanf(str2, "%u", &D(value2,std));
          break;
        case HEX:
          sscanf(str2, "%x", &D(value2,std));
          break;
      }

      CALL_METHOD(twist_table, std);
      goto end;
    }
  }

error:
  CALL_METHOD(print_help, std);
  exit(EXIT_FAILURE);

end:
  free(str1);
  free(str2);
  return 0;
}

/*
 * COLUMN3a = COLUMN1a ^ COLUMN2a
 * COLUMN4a = COLUMN3a; rol32(COLUMN4a, 3)
 * COLUMN1b = ~COLUMN4a
 * COLUMN2b = COLUMN2a; ror32(COLUMN2b, 1)
 */
DEFINE_METHOD(twist_table, std)
{
  int n;
  uint32_t value1a = D(value1,std);
  uint32_t value2a = D(value2,std);
  uint32_t value3a = value1a ^ value2a;
  uint32_t value4a = rol32(value3a, 3);

  printf("{ 0x%.8X, 0x%.8X, 0x%.8X, 0x%.8X },\n",
          value1a, value2a, value3a, value4a);

  /* begin loop */
  for (n=0; n < D(iterations,std)-1; n++) {
    value1a = ~value4a;
    value2a = ror32(value2a, 1);
    value3a = value1a ^ value2a;
    value4a = rol32(value3a, 3);

    printf("{ 0x%.8X, 0x%.8X, 0x%.8X, 0x%.8X },\n",
            value1a, value2a, value3a, value4a);
  }

}

void print_help()
{
  puts("twist");
  puts("generate twist table\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\tyou are here");
  puts("  -a, --value1\t\tvalue 1 (required)");
  puts("  -b, --value2\t\tvalue 2 (required)");
  puts("  -i, --iterations\tnumber of iterations");
  puts("  -d, --decimal\t\tinput values are in base-10");
  puts("  -H, --hexadecimal\tinput values are in base-16 (default)");
}
