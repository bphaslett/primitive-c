# variables
PREFIX=/usr/local
CFLAGS += -O2
CC := gcc

PRIMITIVE_C := io-test getbits xxd-formatter twist primes

ALL_TARGETS := $(PRIMITIVE_C) $(CXX_TARGETS)
all: $(ALL_TARGETS)

$(PRIMITIVE_C):%:%.c primitive_c.h
	$(CC) $(CFLAGS) -o $@ $<

install:
	install -t $(PREFIX)/bin xxd-formatter
	install -t $(PREFIX)/bin getrand

clean: $(ALL_TARGETS)
	for p in $?; do \
	[ ! -e $$p ] || rm $$p; \
	done
