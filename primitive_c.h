#ifndef _PRIMITIVE_C_H
#define _PRIMITIVE_C_H

/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * primitive c classes header file
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <stdint.h>   // for uint32_t, uint64_t
#include <sys/mman.h>   // for mmap/munmap

/*
 * create a (primitive) class
 *
 * Every class gets a custom type both for itself and its data.
 *
 * The class contains a function pointer which is used for calling methods, and
 * a place to store data.
 *
 */
#define CLASS(NAME)                   \
  typedef struct class_##NAME##_s class_##NAME##_t;   \
  typedef struct data_##NAME##_s data_##NAME##_t;   \
                            \
  struct class_##NAME##_s {               \
    void (*call)(class_##NAME##_t*);        \
    data_##NAME##_t *data;              \
  };

/*
 * Defines the class data section
 */
#define DATA(CLASS)   struct data_##CLASS##_s


/*
 * Initialize the class
 */
#define INIT(NAME)                    \
  data_##NAME##_t data_##NAME;            \
  class_##NAME##_t class_##NAME = {           \
    NULL, &data_##NAME                \
  }, *NAME = &class_##NAME;

/*
 * Declare a class method
 * This is the prototype
 */
#define DECLARE_METHOD(NAME, CLASS)           \
  void NAME(class_##CLASS##_t*)
/*
 * Define a class method
 */
#define DEFINE_METHOD(NAME, CLASS)            \
  void NAME(class_##CLASS##_t *CLASS)

/*
 * Call a class method
 */
#define CALL_METHOD(NAME, CLASS)            \
  CLASS->call= (void*) &NAME;             \
  CLASS->call(CLASS);                 \
  CLASS->call=NULL;
/*
 * Call the method of a friend class
 */
#define CALL_METHODF(NAME, CLASS, FRIEND)         \
  CLASS->data->FRIEND->call= (void*) &NAME;       \
  CLASS->data->FRIEND->call(CLASS->data->FRIEND);   \
  CLASS->data->FRIEND->call=NULL;

/*
 * Define a friend class
 */
#define FRIEND(CLASS)    class_##CLASS##_t *CLASS
/*
 * Do a "handshake" with a friend class
 */
#define FRIEND_SHAKE(CLASS,FRIEND)     CLASS->data->FRIEND = FRIEND;

/*
 * Access the class data section
 */
#define D(NAME, CLASS)   CLASS->data->NAME
/*
 * Access the class data section of a friend class
 */
#define DF(NAME, CLASS, FRIEND)  CLASS->data->FRIEND->data->NAME

#endif  /* _PRIMITIVE_C_H */
