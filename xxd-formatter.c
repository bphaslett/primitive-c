/*
 * Copyright (C) 2016-2019 Brian Haslett
 *
 * formats a string of hexadecimal (or shell) code that can be piped to xxd, for
 * a reverse hex dump.
 * (uses primitive c classes)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

// program can be used as thus: 
// xxd-formatter -s [code] | xxd -r | udcli -64


#include <stdio.h>    // printf
#include <stdint.h>   // uint8_t, uint32_t, uint64_t
#include <stdlib.h>   // exit
#include <string.h>   // strlen
#include <getopt.h>   // getopt, getopt_long, struct option
#include <stdbool.h>
#include <ctype.h>    // isalpha

#include "primitive_c.h"   // primitive c classes

#define ishex(x) \
  ({ \
    char _c=c; \
    (isalnum(_c) && \
     ((_c >= 'a' && _c <= 'f') || \
      (_c >= 'A' && _c <= 'F') || \
      (_c >= '0' && _c <= '9'))); \
  })

enum __attribute__ ((__packed__)) bit_width {
  x86=0,
  x64=1
};

static struct option long_options[] = {
  { "width", 1, NULL, 'w' },
  { "string", 1, NULL, 's' },
  { "help", 0, NULL, 'h' },
  { NULL, 0, NULL, 0 }
};

CLASS(std);
DATA(std) {
  uint8_t *str1;
  uint64_t str1_len;
  enum bit_width width;
};
void print_help();
DECLARE_METHOD(parse_string,std);
DECLARE_METHOD(clean_string,std);

int main(int argc, char *argv[])
{
  bool has_string = false, has_width = false;
  uint16_t width;
  uint32_t opt;
  INIT(std);

  if (argc < 2) {
    CALL_METHOD(print_help, std);
    exit(EXIT_FAILURE);
  }

  while ((opt = getopt_long(argc, argv, "hs:w:", long_options, NULL)) != -1) {
    switch (opt) {
      case 'w':
        width=atoi(optarg);
        has_width=1;
        break;
      case 's':
        D(str1_len,std)=strlen(optarg);
        D(str1,std) = malloc(D(str1_len,std)+1);
        strcpy(D(str1,std), optarg);
        has_string = true;
        break;
      case 'h':
        CALL_METHOD(print_help, std);
        exit(EXIT_SUCCESS);
    }
  }

  if (has_width == true && width == 64)
    D(width,std) = x64;
  else
    D(width,std) = x86;
  
  if (has_string == true) {
    CALL_METHOD(clean_string, std);
    CALL_METHOD(parse_string, std);
  }

  free(D(str1,std));
  return 0;
}

DEFINE_METHOD(clean_string, std)
{
  uint8_t c;
  uint32_t i, oldlen=D(str1_len,std), newlen=0;

  // find invalid characters
  for (i=0; i < oldlen; i++) {
    c = D(str1[i],std);
    if (ishex(c)) {
      newlen++;
    }
  }
  if (newlen == oldlen) { return; }

  uint8_t str2[newlen+1]; // the reason we loop twice
  for (i=0, newlen=0; i < oldlen; i++) {
    c = D(str1[i],std);
    if (ishex(c)) {
      str2[newlen] = c;
      newlen++;
    }
  }

  str2[++newlen]='\0';
  memset(D(str1,std), 0, oldlen);
  strcpy(D(str1,std), str2);
  D(str1[newlen],std)='\0';
}

DEFINE_METHOD(parse_string, std)
{
  uint16_t line_sz=32, line_width;
  uint32_t byte, line, realbyte=0, base16=0;

  if (D(width,std) == x64)
    line_sz=64;
    
  line_width=line_sz/2;

  /*
   * hexadecimal counter
   *
   * line is which line we're on
   * byte is which byte we're on for that particular line
   * realbyte is which byte in the entire string
   */
  for (line=0; D(str1[realbyte],std) != '\0'; line++) {
    printf("%08x: ", base16);

    /*
     * byte counter
     *
     * every line has line_sz bytes
     */
    for (byte=0; D(str1[realbyte],std) != '\0'; byte++, realbyte++) {
      if (byte >= D(str1_len,std) || byte >= line_sz) break;
      printf("%c", D(str1[realbyte],std));
    }

    puts(""); 
    base16+=line_width; // bit_width determines line_width

    if (realbyte >= D(str1_len,std)) break;
  }

}

void print_help()
{
  puts("xxd-formatter");
  puts("formats a string of hexadecimal (or shell) code that can be piped to");
  puts("xxd for a reverse hex dump.\n");
  puts("Usage: [options]\n");
  puts("Options:");
  puts("  -h, --help\t\t\tyou are here");
  puts("  -s, --string=[STRING]\t\thexadecimal string");
  puts("  -w, --width=[SIZE]\t\tbit width, 64 or 32 (default)");
}
